package com.brucemax.hungrytraveler.model;

import com.google.android.gms.maps.model.Marker;

import java.util.Comparator;

public class PlaceItem {

    private String placeId;
    private Marker placeMarker;
    private int likesNumber;

    public PlaceItem (String placeId, Marker placeMarker) {
        this.placeId = placeId;
        this.placeMarker = placeMarker;
    }

    public Marker getPlaceMarker() {
        return placeMarker;
    }

    public int getLikesNumber() {
        return likesNumber;
    }

    public String getLikesNumberString() {
        if (likesNumber > 0) {
            return "Likes "+ String.valueOf(likesNumber);
        } else {
            return "No likes";
        }
    }

    public String getPlaceId() {
        return placeId;
    }

    public void setPlaceId(String placeId) {
        this.placeId = placeId;
    }

    public void setPlaceMarker(Marker placeMarker) {
        this.placeMarker = placeMarker;
    }

    public void setLikesNumber(int likesNumber) {
        this.likesNumber = likesNumber;
        if (placeMarker != null) {
            placeMarker.setSnippet(getLikesNumberString());
        }
    }

    public static class LikesComparator implements Comparator<PlaceItem> {
        @Override
        public int compare(PlaceItem o1, PlaceItem o2) {
            return o1.getLikesNumber() > o2.getLikesNumber() ? -1
                    : o1.getLikesNumber() < o2.getLikesNumber() ? 1
                    : 0;
        }
    }
}
