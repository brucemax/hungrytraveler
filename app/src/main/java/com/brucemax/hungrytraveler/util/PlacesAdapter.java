package com.brucemax.hungrytraveler.util;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.ArrayList;
import java.util.HashMap;

public class PlacesAdapter extends ArrayAdapter implements Filterable {

    public static int DESTINATION_TYPE = 0;
    public static int DEPARTURE_TYPE = 1;

    private ArrayList<String> resultList;
    private HashMap<String, String> resultMap; // contains place reference for getting location
    private int mType; // for recognize view in OnClickItemListener

    public PlacesAdapter(Context context, int textViewResourceId, int type) {
        super(context, textViewResourceId);
        this.mType = type;
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public String getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    // Retrieve the autocomplete results.
                    //TODO check enough pause time to prevent a huge number of requests
                    resultMap = PlaceHelper.autocomplete(constraint.toString());
                    resultList = new ArrayList<>(resultMap.keySet());

                    // Assign the data to the FilterResults
                    filterResults.values = resultList;
                    filterResults.count = resultList.size();
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    notifyDataSetChanged();
                }
                else {
                    notifyDataSetInvalidated();
                }
            }};
        return filter;
    }

    public String getPlaceReference(String place) {
        return resultMap.get(place);
    }

    public int getType() {
        return mType;
    }
}
