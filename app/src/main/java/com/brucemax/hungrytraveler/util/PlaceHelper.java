package com.brucemax.hungrytraveler.util;

import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;

public class PlaceHelper {

    private static final String LOG_TAG = PlaceHelper.class.getSimpleName();
    public static final String GOOGLE_API_KEY = "AIzaSyBtOpnswP9lWMjbsGH8mbaNt9XAi5fazvs";

    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String TYPE_DETAILS = "/details";
    private static final String OUT_JSON = "/json";

    public interface PlaceLocationListener {
        void locationReceived(LatLng location);
    }

    public static HashMap<String, String> autocomplete(String input) {
        HashMap<String, String> resultMap = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?sensor=false&key=" + GOOGLE_API_KEY);
           // sb.append("&components=country:ee");
            sb.append("&types=(cities)&language=en");
            sb.append("&input=").append(URLEncoder.encode(input, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return null;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return null;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultMap = new HashMap<>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                resultMap.put(predsJsonArray.getJSONObject(i).getString("description"),
                        predsJsonArray.getJSONObject(i).getString("reference"));

            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultMap;
    }

    public static void getPlaceLocation(String placeReference, PlaceLocationListener locationListener) {
        new GetPlaceDetailsTask(locationListener).execute(placeReference);
    }

    private static LatLng getLocationByGoogleReference(String placeReference) {
        LatLng location = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_DETAILS + OUT_JSON);
            sb.append("?sensor=false&key=" + GOOGLE_API_KEY);
            sb.append("&language=en");
            sb.append("&reference=").append(URLEncoder.encode(placeReference, "utf8"));

            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return null;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return null;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());

            // get location from response
            JSONObject locationObject = jsonObj.getJSONObject("result").getJSONObject("geometry").getJSONObject("location");
            location = new LatLng(Double.parseDouble(locationObject.getString("lat")),
                    Double.parseDouble(locationObject.getString("lng")));

        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return location;
    }

    private static class GetPlaceDetailsTask extends AsyncTask<String, Void, LatLng> {

        private PlaceLocationListener mPlaceLocationListener;

        GetPlaceDetailsTask(PlaceLocationListener placeLocationListener) {
            this.mPlaceLocationListener = placeLocationListener;
        }

        @Override
        protected LatLng doInBackground(String... arg) {
            return getLocationByGoogleReference(arg[0]);
        }

        @Override
        protected void onPostExecute(LatLng result) {
            // call listener for send result
            mPlaceLocationListener.locationReceived(result);
        }
    }

}
