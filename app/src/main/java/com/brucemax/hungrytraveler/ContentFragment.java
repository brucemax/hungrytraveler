package com.brucemax.hungrytraveler;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;

import com.brucemax.hungrytraveler.util.PlaceHelper;
import com.brucemax.hungrytraveler.util.PlacesAdapter;
import com.google.android.gms.maps.model.LatLng;

/**
 * Main content fragment.
 * Note. Fragment is more that we need in this app now. But it is fine for easily scalable system in future.
 */
public class ContentFragment extends Fragment /*implements OnMapReadyCallback, RoutingListener */{

    private AutoCompleteTextView actvDeparturePlace, actvDestionationPlace;

    private CustomMapFragment mapFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        PlaceCompleteListener departureCompleteListener = new PlaceCompleteListener();

        actvDeparturePlace = (AutoCompleteTextView) rootView.findViewById(R.id.actvDeparturePlace);
        actvDeparturePlace.setAdapter(new PlacesAdapter(getActivity(), R.layout.auto_complete_item, PlacesAdapter.DEPARTURE_TYPE));
        actvDeparturePlace.setOnItemClickListener(departureCompleteListener);

        actvDestionationPlace = (AutoCompleteTextView) rootView.findViewById(R.id.actvDestinationPlace);
        actvDestionationPlace.setAdapter(new PlacesAdapter(getActivity(), R.layout.auto_complete_item, PlacesAdapter.DESTINATION_TYPE));
        actvDestionationPlace.setOnItemClickListener(departureCompleteListener);

        mapFragment = (CustomMapFragment) getActivity().getFragmentManager().findFragmentById(R.id.map);
        mapFragment.setRetainInstance(true); // not recreate map during rotation
        mapFragment.getMapAsync(mapFragment);
        return rootView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_clear) {
            actvDeparturePlace.setText("");
            actvDeparturePlace.requestFocus();
            actvDestionationPlace.setText("");
            mapFragment.clearMap();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class PlaceCompleteListener implements AdapterView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            final String chosePlace = (String) parent.getItemAtPosition(position);
            final PlacesAdapter placesAdapter =  (PlacesAdapter) parent.getAdapter();
            String placeReference = placesAdapter.getPlaceReference(chosePlace);
            PlaceHelper.getPlaceLocation(placeReference, new PlaceHelper.PlaceLocationListener() {
                @Override
                public void locationReceived(LatLng loc) {

                    if (placesAdapter.getType() == PlacesAdapter.DEPARTURE_TYPE) {
                        mapFragment.setDeparture(chosePlace, loc);
                    } else if (placesAdapter.getType() == PlacesAdapter.DESTINATION_TYPE) {
                        mapFragment.setDestination(chosePlace, loc);
                    }

                    mapFragment.updateCameraMapPosition();
                }
            });
        }
    }
}
