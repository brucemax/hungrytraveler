package com.brucemax.hungrytraveler;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.brucemax.hungrytraveler.model.PlaceItem;
import com.brucemax.hungrytraveler.route.Route;
import com.brucemax.hungrytraveler.route.Routing;
import com.brucemax.hungrytraveler.route.RoutingListener;
import com.brucemax.hungrytraveler.util.Utils;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.RequestBatch;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.model.GraphObject;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CustomMapFragment extends MapFragment implements OnMapReadyCallback, RoutingListener {

    private Marker mDepartureMarker, mDestinationMarker;
    private Marker middlePointMarker;
    private Map<String, PlaceItem> placeMarkersList = new HashMap<>(); // contain ids and Place item of restaurants/cafes

    private GoogleMap mMap;
    private Polyline mPolylineRoute;

    private ProgressDialog progressDialog;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        googleMap.setMyLocationEnabled(true);
    }


    @Override
    public void onRoutingFailure() {
        //hide some progress bar
        progressDialog.dismiss();
        Toast.makeText(getActivity(), "Unable to get directions", Toast.LENGTH_LONG).show();
        if (mPolylineRoute != null) {
            mPolylineRoute.remove();
        }
    }

    @Override
    public void onRoutingStart() {
        //show some progress bar
        progressDialog = ProgressDialog.show(getActivity(), "Please wait..", "Build rout", true);
    }

    @Override
    public void onRoutingSuccess(PolylineOptions mPolyOptions, Route route) {
        //hide progress bar
        progressDialog.dismiss();
        PolylineOptions polyoptions = new PolylineOptions();
        polyoptions.color(Color.BLUE);
        polyoptions.width(7);
        polyoptions.addAll(mPolyOptions.getPoints());

        mPolylineRoute = mMap.addPolyline(polyoptions);

        LatLng point = Route.getPointAtDistance(route.getLength() / 2, polyoptions.getPoints());

        if (middlePointMarker != null) {
            middlePointMarker.setPosition(point);
        } else {
            middlePointMarker = mMap.addMarker(new MarkerOptions().position(point).title("Yum!").
                    anchor(0.5f, 0.5f).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_middle_point)));
        }

        // Create params for facebook request
        Bundle params = new Bundle();
        params.putString("type", "place");
        //params.putString("center", "37.76,-122.427"); // for test
        //params.putString("center", "59.36733,26.50701");
        params.putString("center", point.latitude+","+point.longitude);
        Log.d("myTag", "point = " + point.toString());
        params.putString("q", "restaurant");
        params.putString("distance", "50000");
        params.putString("limit", "10");

        Session currentSession = ((MainActivity) getActivity()).getSession();
        if (currentSession != null && currentSession.isOpened()) {
            requestToFacebookApi(params); // request to Fecebook for places
        } else {
            Toast.makeText(getActivity(), "Please re enter in app and login in facebook", Toast.LENGTH_LONG).show();
        }
    }

    public void setDeparture(String title, LatLng loc) {
        if (mDepartureMarker == null) {
            mDepartureMarker = mMap.addMarker(new MarkerOptions().position(loc).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_city_marker)));
        }
        mDepartureMarker.setPosition(loc);
        mDepartureMarker.setTitle(title);
    }

    public void setDestination(String title, LatLng loc) {
        if (mDestinationMarker == null) {
            mDestinationMarker = mMap.addMarker(new MarkerOptions().position(loc).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_city_marker)));
        }
        mDestinationMarker.setPosition(loc);
        mDestinationMarker.setTitle(title);
    }

    public void updateCameraMapPosition() {

        if(mDepartureMarker != null && mDestinationMarker != null) {
            LatLngBounds.Builder bc = new LatLngBounds.Builder();
            bc.include(mDepartureMarker.getPosition());
            bc.include(mDestinationMarker.getPosition());
            mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bc.build(), 150));

            // disable keyboard
            Utils.hideKeyboard(getActivity());

            // delete old markers
            for(PlaceItem placeItem : placeMarkersList.values()) {
                placeItem.getPlaceMarker().remove();
            }
            placeMarkersList.clear();

            showRoute(mDepartureMarker.getPosition(), mDestinationMarker.getPosition());

            if (mPolylineRoute != null) {
                mPolylineRoute.remove();
            }

        } else if (mDepartureMarker != null) {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(mDepartureMarker.getPosition())      // Sets the center of the map to Mountain View
                    .zoom(8)                   // Sets the zoom
                            //        .bearing(90)                // Sets the orientation of the camera to east
                            //        .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        } else if (mDestinationMarker != null) {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(mDestinationMarker.getPosition())      // Sets the center of the map to Mountain View
                    .zoom(8)                   // Sets the zoom
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    public void clearMap() {
        if (mMap != null) {
            mMap.clear();
        }

        mDestinationMarker = null;
        mDepartureMarker = null;
    }

    private void requestToFacebookApi(Bundle params) {
        Request request = new Request(((MainActivity) getActivity()).getSession(), "search", params, HttpMethod.GET, new Request.Callback() {
            @Override
            public void onCompleted(Response response) {
                JSONArray placeJsonArray;
                try {
                    placeJsonArray = response.getGraphObject().getInnerJSONObject().getJSONArray("data");

                    @SuppressWarnings("MismatchedQueryAndUpdateOfCollection")
                    RequestBatch requestBatch = new RequestBatch();

                    for (int i = 0; i < placeJsonArray.length(); i++) {
                        JSONObject placeObject = placeJsonArray.getJSONObject(i);
                        String id = placeObject.getString("id"); // for request likes in future
                        String name = placeObject.getString("name");
                        JSONObject location = placeObject.getJSONObject("location");
                        LatLng point = new LatLng(location.getDouble("latitude"), location.getDouble("longitude"));
                        placeMarkersList.put(id, new PlaceItem(id, mMap.addMarker(new MarkerOptions().position(point).title(name))));

                        Bundle params = new Bundle();
                        params.putString("id", id);
                        params.putString("fields", "likes");
                        requestBatch.add(new Request(((MainActivity) getActivity()).getSession(), "search", params, HttpMethod.GET, new Request.Callback() {
                            @Override
                            public void onCompleted(Response response) {
                                // collect likes
                                JSONArray placeJsonArray;
                                try {
                                    GraphObject graphLikesObject = response.getGraphObject();

                                    if (graphLikesObject != null) {
                                        placeJsonArray = graphLikesObject.getInnerJSONObject().getJSONArray("data");
                                        if (placeJsonArray.length() > 0) {
                                            JSONObject likeObject = placeJsonArray.getJSONObject(0);
                                            int likes = likeObject.getInt("likes");
                                            String id = likeObject.getString("id");
                                            PlaceItem placeItem = placeMarkersList.get(id);

                                            placeItem.setLikesNumber(likes);
                                        }
                                    }

                                    //Log.d("myTag", "places number " + placeJsonArray.length());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }));
                    }

                    requestBatch.addCallback(new RequestBatch.Callback() {
                        @Override
                        public void onBatchCompleted(RequestBatch requests) {
                            Log.d("myTag", "batch callback ");
                            // handle markers
                            handleMarkersColorAccordingLikes(new ArrayList<>(placeMarkersList.values()));
                        }
                    });

                    // Get info for all places and show marker color depending on number of likes
                    requestBatch.executeAsync();

                    Log.d("myTag", "places number " + placeJsonArray.length());
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
        RequestAsyncTask task = new RequestAsyncTask(request);
        task.execute();
    }

    private void showRoute(LatLng departure, LatLng destination) {
        //TODO create option for choosing travel mode
        Routing routing = new Routing(Routing.TravelMode.DRIVING);
        routing.registerListener(this);
        routing.execute(departure, destination);

        if (middlePointMarker != null) {
            middlePointMarker.remove();
        }
    }

    /**
     * Method take a list of {@link PlaceItem}, sort it
     * and set color of marker (hue) according to number of likes for place
     * @param placeItems list of {@link PlaceItem}
     */
    private void handleMarkersColorAccordingLikes (List<PlaceItem> placeItems) {
        Collections.sort(placeItems, new PlaceItem.LikesComparator());

        int minLikes = placeItems.get(0).getLikesNumber();
        int maxLikes = placeItems.get(placeItems.size()-1).getLikesNumber();

        for (PlaceItem placeItem : placeItems) {
            int likes = placeItem.getLikesNumber();
            // calculate color according number of likes
            int hue = 250 * (likes - minLikes)/(maxLikes - minLikes); // color set (0..360), 250 - dark blue

            placeItem.getPlaceMarker().setIcon(BitmapDescriptorFactory.defaultMarker(hue));
        }
    }
}
