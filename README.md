# README #

This app use for find restaurants and cafe on the middle of way between two cities.

### What is this repository for? ###

* Test app "Hungry Traveler"
* Version 0.1
* [Maks Pipkin](https://bitbucket.org/brucemax)

### How do I get set up? ###

* Download apk file from repo
* Install app to device
* Run app and login in shown facebook form
* Use!

![device-2015-03-02-230828.png](https://bitbucket.org/repo/4jgxEo/images/1924836290-device-2015-03-02-230828.png)

![device-2015-03-02-231508.png](https://bitbucket.org/repo/4jgxEo/images/2560289740-device-2015-03-02-231508.png)

Color of marker depend on number of likes (from blue to red)